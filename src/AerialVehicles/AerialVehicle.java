package AerialVehicles;


import AerialVehicles.enums.PlaneStatus;
import Capabilities.Missionable;
import Entities.Coordinates;

public abstract class AerialVehicle implements Missionable {
    private int HoursSinceTreatment;
    private PlaneStatus planeStatus;
    private Coordinates mainBase;
    private int TreatmentFrequencyHrs;

    public AerialVehicle(Coordinates mainBase, int treatmentFrequencyInHrs) {
        this.HoursSinceTreatment = 0;
        this.planeStatus = PlaneStatus.Ready;
        this.mainBase = mainBase;
        this.TreatmentFrequencyHrs = treatmentFrequencyInHrs;
    }

    public void setPlaneStatus(PlaneStatus planeStatus) {
        this.planeStatus = planeStatus;
    }

    public PlaneStatus getPlaneStatus() {
        return planeStatus;
    }

    public void flyTo(Coordinates destination) {
        switch (this.planeStatus) {
            case Ready:
                System.out.println("Flying to: " + destination.toString());
                this.planeStatus = PlaneStatus.InAir;
                break;

            case NotReady:
                System.out.println("Aerial Vehicle is not ready to fly. ");

                break;

            case InAir:
                System.out.println("Aerial Vehicle already in air. ");
        }
    };

    public void land(Coordinates destination) {
        System.out.println("Landing on " + destination.toString());
        updateStatus();
    }

    private void updateStatus() {
        if(this.HoursSinceTreatment >= this.TreatmentFrequencyHrs) {
            this.planeStatus = PlaneStatus.NotReady;
            repair();
        } else {
            this.planeStatus = PlaneStatus.Ready;
        }
    }

    private void repair() {
        System.out.println("Fixing AerialVehicle...");
        this.planeStatus = PlaneStatus.Ready;
    }


    public Coordinates getMainBase() {
        return mainBase;
    }
}
