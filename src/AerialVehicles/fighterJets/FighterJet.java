package AerialVehicles.fighterJets;

import AerialVehicles.AerialVehicle;
import AerialVehicles.enums.PlaneStatus;
import Capabilities.Attackable;
import Capabilities.enums.MissileType;
import Entities.Coordinates;

public abstract class FighterJet extends AerialVehicle implements Attackable {
    private static final int treatmentFrequencyInHrs = 250;
    private int missileCount;
    private MissileType missileType;

    public FighterJet(Coordinates mainBase, int missileCount, MissileType missileType) {
        super(mainBase, treatmentFrequencyInHrs);
        this.missileCount = missileCount;
        this.missileType = missileType;
    }

    @Override
    public int getMissileCount() {
        return this.missileCount;
    }

    @Override
    public void setMissileCount(int missileCount) {
        this.missileCount = missileCount;
    }

    @Override
    public MissileType getMissileType() {
        return this.missileType;
    }

    @Override
    public void setMissileType(MissileType missileType) {
        this.missileType = missileType;
    }
}
