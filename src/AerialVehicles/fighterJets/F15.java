package AerialVehicles.fighterJets;


import AerialVehicles.enums.PlaneStatus;
import Capabilities.Intelligable;
import Capabilities.enums.MissileType;
import Capabilities.enums.SensorType;
import Entities.Coordinates;

public class F15 extends FighterJet implements Intelligable {
    private SensorType sensorType;

    public F15(Coordinates mainBase,
               int missileCount, MissileType missileType, SensorType sensorType) {
        super(mainBase, missileCount, missileType);

        this.sensorType = sensorType;
    }

    @Override
    public SensorType getSensorType() {
        return this.sensorType;
    }

    @Override
    public void setSensorType(SensorType sensorType) {
        this.sensorType = sensorType;
    }
}
