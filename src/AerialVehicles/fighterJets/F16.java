package AerialVehicles.fighterJets;


import AerialVehicles.enums.PlaneStatus;
import Capabilities.Bdaable;
import Capabilities.enums.CameraType;
import Capabilities.enums.MissileType;
import Entities.Coordinates;

public class F16 extends FighterJet implements Bdaable {
    private CameraType cameraType;

    public F16(Coordinates mainBase, int missileCount,
               MissileType missileType, CameraType cameraType) {
        super(mainBase, missileCount, missileType);

        this.cameraType = cameraType;
    }

    @Override
    public CameraType getCameraType() {
        return this.cameraType;
    }

    @Override
    public void setCameraType(CameraType cameraType) {
        this.cameraType = cameraType;
    }
}
