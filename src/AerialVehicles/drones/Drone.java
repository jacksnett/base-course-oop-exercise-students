package AerialVehicles.drones;

import AerialVehicles.AerialVehicle;
import AerialVehicles.enums.PlaneStatus;
import Capabilities.Intelligable;
import Capabilities.enums.SensorType;
import Entities.Coordinates;

public abstract class Drone extends AerialVehicle implements Intelligable {
    private SensorType sensorType;

    public Drone(Coordinates mainBase, int treatmentFrequencyInHrs,
                 SensorType sensorType) {
        super(mainBase, treatmentFrequencyInHrs);
        this.sensorType = sensorType;
    }


    @Override
    public SensorType getSensorType() {
        return this.sensorType;
    }

    @Override
    public void setSensorType(SensorType sensorType) {
        this.sensorType = sensorType;
    }

    public void hoverOverLocation(Coordinates destination) {
        this.setPlaneStatus(PlaneStatus.InAir);

        System.out.println("Hovering over: " + destination.toString());
    }
}
