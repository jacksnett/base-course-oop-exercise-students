package AerialVehicles.drones.hermes;

import AerialVehicles.drones.Drone;
import AerialVehicles.enums.PlaneStatus;
import Capabilities.Bdaable;
import Capabilities.enums.CameraType;
import Capabilities.enums.SensorType;
import Entities.Coordinates;

public abstract class Hermes extends Drone implements Bdaable {
    private static final int treatmentFrequencyInHrs = 100;
    private CameraType cameraType;

    public Hermes(Coordinates mainBase,
                  SensorType sensorType, CameraType cameraType) {
        super(mainBase, treatmentFrequencyInHrs, sensorType);
        this.cameraType = cameraType;
    }

    public CameraType getCameraType() {
        return this.cameraType;
    }

    public void setCameraType(CameraType cameraType) {
        this.cameraType = cameraType;
    }
}
