package AerialVehicles.drones.hermes;


import AerialVehicles.drones.hermes.Hermes;
import AerialVehicles.enums.PlaneStatus;
import Capabilities.Bdaable;
import Capabilities.enums.CameraType;
import Capabilities.enums.SensorType;
import Entities.Coordinates;

public class Zik extends Hermes implements Bdaable {
    public Zik(Coordinates mainBase, SensorType sensorType, CameraType cameraType) {
        super(mainBase, sensorType, cameraType);
    }
}