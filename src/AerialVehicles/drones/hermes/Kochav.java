package AerialVehicles.drones.hermes;

import AerialVehicles.drones.hermes.Hermes;
import AerialVehicles.enums.PlaneStatus;
import Capabilities.*;
import Capabilities.enums.CameraType;
import Capabilities.enums.MissileType;
import Capabilities.enums.SensorType;
import Entities.Coordinates;

public class Kochav extends Hermes implements Attackable {
    private int missileCount;
    private MissileType missileType;

    public Kochav(Coordinates mainBase,
                  SensorType sensorType, CameraType cameraType, int missileCount, MissileType missileType) {
        super(mainBase, sensorType, cameraType);
        this.missileCount = missileCount;
        this.missileType = missileType;
    }

    @Override
    public int getMissileCount() {
        return this.missileCount;
    }

    @Override
    public void setMissileCount(int missileCount) {
        this.missileCount = missileCount;
    }

    @Override
    public MissileType getMissileType() {
        return this.missileType;
    }

    @Override
    public void setMissileType(MissileType missileType) {
        this.missileType = missileType;
    }
}
