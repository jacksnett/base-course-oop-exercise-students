package AerialVehicles.drones.haron;

import AerialVehicles.drones.Drone;
import AerialVehicles.enums.PlaneStatus;
import Capabilities.Attackable;
import Capabilities.enums.MissileType;
import Capabilities.enums.SensorType;
import Entities.Coordinates;

public abstract class Haron extends Drone implements Attackable {
    private static final int treatmentFrequencyInHrs = 150;
    private int missileCount;
    private MissileType missileType;

    public Haron(Coordinates mainBase,
                 SensorType sensorType, int missileCount, MissileType missileType) {
        super(mainBase, treatmentFrequencyInHrs, sensorType);

        this.missileCount = missileCount;
        this.missileType = missileType;
    }

    @Override
    public int getMissileCount() {
        return this.missileCount;
    }

    @Override
    public void setMissileCount(int missileCount) {
        this.missileCount = missileCount;
    }

    @Override
    public MissileType getMissileType() {
        return this.missileType;
    }

    @Override
    public void setMissileType(MissileType missileType) {
        this.missileType = missileType;
    }
}

