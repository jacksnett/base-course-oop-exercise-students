package AerialVehicles.drones.haron;


import AerialVehicles.drones.haron.Haron;
import AerialVehicles.enums.PlaneStatus;
import Capabilities.*;
import Capabilities.enums.CameraType;
import Capabilities.enums.MissileType;
import Capabilities.enums.SensorType;
import Entities.Coordinates;

public class Shoval extends Haron implements Bdaable {
    private CameraType cameraType;

    public Shoval(Coordinates mainBase,
                  SensorType sensorType, int missileCount, MissileType missileType, CameraType cameraType) {
        super(mainBase, sensorType, missileCount, missileType);
        this.cameraType = cameraType;
    }

    @Override
    public CameraType getCameraType() {
        return this.cameraType;
    }

    @Override
    public void setCameraType(CameraType cameraType) {
        this.cameraType = cameraType;
    }
}

