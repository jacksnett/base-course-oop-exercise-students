package AerialVehicles.drones.haron;

import AerialVehicles.drones.haron.Haron;
import AerialVehicles.enums.PlaneStatus;
import Capabilities.Attackable;
import Capabilities.enums.MissileType;
import Capabilities.enums.SensorType;
import Entities.Coordinates;

public class Eitan extends Haron implements Attackable {
    public Eitan(Coordinates mainBase, SensorType sensorType, int missileCount, MissileType missileType) {
        super(mainBase, sensorType, missileCount, missileType);
    }
}
