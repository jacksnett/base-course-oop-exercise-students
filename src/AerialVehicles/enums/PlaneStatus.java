package AerialVehicles.enums;

public enum PlaneStatus {
    Ready,
    NotReady,
    InAir
}
