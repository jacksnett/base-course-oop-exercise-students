import AerialVehicles.drones.haron.Eitan;
import AerialVehicles.fighterJets.F16;
import Capabilities.enums.CameraType;
import Capabilities.enums.MissileType;
import Capabilities.enums.SensorType;
import Entities.Coordinates;
import Missions.AttackMission;
import Missions.BdaMission;
import Missions.IntelligenceMission;
import exeptions.AerialVehicleNotCompatibleException;

public class RunTests {
    public static void runIntelligenceMission() {
        Coordinates homeCoordinates = new Coordinates(31.827604665263365, 34.81739714569337);
        Coordinates missionCoordinates = new Coordinates(33.2033427805222, 44.5176910795946);
        Eitan eitan = new Eitan(homeCoordinates,
                SensorType.InfraRed, 4, MissileType.Python);

        eitan.flyTo(missionCoordinates);
        eitan.hoverOverLocation(missionCoordinates);

        try {
            IntelligenceMission intelligenceMission =
                    new IntelligenceMission(missionCoordinates,
                            "Dror zalica", eitan, "Iraq");
            intelligenceMission.executeMission();
        } catch (AerialVehicleNotCompatibleException e) {
            e.getMessage();
        }

        eitan.land(homeCoordinates);
    }

    public static void runAttackMission() {
        Coordinates homeCoordinates = new Coordinates(31.827604665263365, 34.81739714569337);
        Coordinates missionCoordinates = new Coordinates(33.2033427805222, 44.5176910795946);
        F16 f16 = new F16(homeCoordinates, 250 ,MissileType.Spice250, CameraType.NightVision);

        f16.flyTo(missionCoordinates);

        try {
            AttackMission attackMission = new AttackMission(missionCoordinates, "Ze'ev Raz", f16,
                    "Tuwaitha Nuclear Research Center");

            attackMission.executeMission();
        } catch (AerialVehicleNotCompatibleException e) {
            e.getMessage();
        }

        f16.land(homeCoordinates);
    }

    public static void runBdaMission() {
        Coordinates homeCoordinates = new Coordinates(31.827604665263365, 34.81739714569337);
        Coordinates missionCoordinates = new Coordinates(33.2033427805222, 44.5176910795946);

        F16 f16 = new F16(homeCoordinates, 0, MissileType.Spice250, CameraType.Thermal);

        f16.flyTo(missionCoordinates);

        try {
            BdaMission bdaMission = new BdaMission(missionCoordinates, "Ilan Ramon", f16,
                    "Tuwaitha Nuclear Research Center" );
            bdaMission.executeMission();
        } catch (AerialVehicleNotCompatibleException e) {
            e.getMessage();
        }

        f16.land(homeCoordinates);
    }
}
