package Missions;

import AerialVehicles.AerialVehicle;
import Capabilities.Attackable;
import Capabilities.Bdaable;
import Entities.Coordinates;
import exeptions.AerialVehicleNotCompatibleException;

public class BdaMission extends Mission {
    private String objective;

    public BdaMission(Coordinates targetCoordinates, String pilotName, AerialVehicle executor, String objective) throws AerialVehicleNotCompatibleException {
        super(targetCoordinates, pilotName, executor);
        this.objective = objective;
        this.setMissionMsg(this.getBdaMsg());
    }

    private String getBdaMsg() throws AerialVehicleNotCompatibleException {
        try{
            Bdaable bdaable = (Bdaable) this.getExecutor();
            return  "Taking pictures of suspect" + this.objective + "with: " + bdaable.getCameraType();}
        catch (Exception e) {
            throw new AerialVehicleNotCompatibleException("couldn't covnert from executer to bdaable");
        }
    }
}
