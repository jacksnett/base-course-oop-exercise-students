package Missions;

import AerialVehicles.AerialVehicle;
import Capabilities.Intelligable;
import Entities.Coordinates;
import exeptions.AerialVehicleNotCompatibleException;

public class IntelligenceMission extends Mission {
    private String region;

    public IntelligenceMission(Coordinates targetCoordinates, String pilotName, AerialVehicle executor, String region) throws AerialVehicleNotCompatibleException {
        super(targetCoordinates, pilotName, executor);
        this.region = region;
        this.setMissionMsg(getIntellMsg());
    }

    private String getIntellMsg() throws AerialVehicleNotCompatibleException {
        try{
            Intelligable intelligable = (Intelligable) this.getExecutor();
            return  "Collecting Data in " + this.region + "with: sensor type: " + intelligable.getSensorType();
        }
        catch (Exception e) {
            throw new AerialVehicleNotCompatibleException("couldn't covnert from attackable to executer");
        }
    }
}
