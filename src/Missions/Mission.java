package Missions;

import AerialVehicles.AerialVehicle;
import Capabilities.Missionable;
import Entities.Coordinates;

public abstract class Mission{
    private static final String defaultMissionMsg = "Executing mission ";
    private Coordinates targetCoordinates;
    private String pilotName;
    private AerialVehicle executor;
    private String missionMsg;

    public Mission(Coordinates targetCoordinates, String pilotName, AerialVehicle executor) {
        this.targetCoordinates = targetCoordinates;
        this.pilotName = pilotName;
        this.executor = executor;
        this.missionMsg = defaultMissionMsg;
    }

    public AerialVehicle getExecutor() {
        return executor;
    }

    public void setExecutor(AerialVehicle executor) {
        this.executor = executor;
    }

    public Coordinates getTargetCoordinates() {
        return targetCoordinates;
    }

    public void setTargetCoordinates(Coordinates targetCoordinates) {
        this.targetCoordinates = targetCoordinates;
    }

    public String getPilotName() {
        return pilotName;
    }

    public void setPilotName(String pilotName) {
        this.pilotName = pilotName;
    }

    public void begin() {
        System.out.println("Beginning da great mission?!, oh yea!");
        this.executor.flyTo(this.targetCoordinates);
    }

    public void cancel() {
        System.out.println("This sucks.. mission canceled. ");
        this.executor.land(this.executor.getMainBase());
    }

    public void performMission() {
        this.executeMission();
        this.executor.land(this.executor.getMainBase());
        System.out.println("Despite the challenges, We finished the mission! ");
    }

    public String getMissionMsg() {
        return missionMsg;
    }

    public void setMissionMsg(String missionMsg) {
        this.missionMsg = missionMsg;
    }

    public void executeMission() {
        System.out.println(this.getPilotName() + ": " + this.getExecutor().getClass().getSimpleName() + " " + this.missionMsg);
    }
}
