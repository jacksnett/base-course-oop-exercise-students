package Missions;

import AerialVehicles.AerialVehicle;
import Capabilities.Attackable;
import Capabilities.enums.MissileType;
import Entities.Coordinates;
import exeptions.AerialVehicleNotCompatibleException;

public class AttackMission extends Mission {

    private String target;

    public AttackMission(Coordinates targetCoordinates, String pilotName, AerialVehicle executor, String target)
            throws AerialVehicleNotCompatibleException {
        super(targetCoordinates, pilotName, executor);

        this.target = target;
        this.setMissionMsg(getAttackMsg());
    }

    private String getAttackMsg() throws AerialVehicleNotCompatibleException {
        try{
            Attackable attackable = (Attackable) this.getExecutor();
            return  "Attacking suspect " + this.target + " with: " + attackable.getMissileType();        }
        catch (Exception e) {
            throw new AerialVehicleNotCompatibleException("couldn't covnert from attackable to executer");
        }
    }
}
