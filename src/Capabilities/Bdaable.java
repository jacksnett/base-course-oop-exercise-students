package Capabilities;

import Capabilities.enums.CameraType;

public interface Bdaable extends Missionable{
    CameraType getCameraType();
    void setCameraType(CameraType cameraType);
}
