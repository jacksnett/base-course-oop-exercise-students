package Capabilities;

import Capabilities.enums.SensorType;

public interface Intelligable extends Missionable{
    SensorType getSensorType();
    void setSensorType(SensorType sensorType);
}
