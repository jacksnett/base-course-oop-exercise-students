package Capabilities;

import Capabilities.enums.MissileType;

public interface Attackable extends Missionable{
    int getMissileCount();
    void setMissileCount(int missileCount);
    MissileType getMissileType();
    void setMissileType(MissileType missileType);
}

