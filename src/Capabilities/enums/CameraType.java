package Capabilities.enums;

public enum CameraType {
    Regular,
    Thermal,
    NightVision
}
